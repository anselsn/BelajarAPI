"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angular = require("angular");
var ryan_validator_1 = require("./ryan-validator");
var uib = require("angular-ui-bootstrap");
var animate = require("angular-animate");
var app = angular.module('aspnet', [ryan_validator_1.default, uib, animate]);
