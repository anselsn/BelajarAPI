"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angular = require("angular");
var angularMessages = require("angular-messages");
var ryanValidator = angular.module('ryan-angular-validator', [angularMessages]);
var ValidatorController = (function () {
    function ValidatorController() {
    }
    ValidatorController.prototype.$onInit = function () {
        var control = window.document.getElementsByName(this.input.$name)[0];
        this.SetFieldTitle();
        this.SetMinErrorMessage(control);
        this.SetMaxErrorMessage(control);
        this.SetMinLengthErrorMessage(control);
        this.SetMaxLengthErrorMessage(control);
        this.SetPatternMismatchMessage();
    };
    ValidatorController.prototype.SetFieldTitle = function () {
        if (!this.title) {
            this.title = this.input.$name;
        }
    };
    ValidatorController.prototype.SetMinErrorMessage = function (control) {
        this.minDesc = 'input value needs to be higher';
        if (control) {
            var min = control.getAttribute('min') || control.getAttribute('ng-min');
            if (min) {
                this.minDesc = 'minimum input value is ' + min;
            }
        }
    };
    ValidatorController.prototype.SetMaxErrorMessage = function (control) {
        this.maxDesc = 'input value needs to be lower';
        if (control) {
            var max = control.getAttribute('max') || control.getAttribute('ng-max');
            if (max) {
                this.maxDesc = 'maximum input value is ' + max;
            }
        }
    };
    ValidatorController.prototype.SetMinLengthErrorMessage = function (control) {
        this.minLengthDesc = 'input length needs to be longer';
        if (control) {
            var minlength = control.getAttribute('minlength') || control.getAttribute('ng-minlength');
            if (minlength) {
                this.minLengthDesc = 'minimum input length is ' + minlength + ' characters';
            }
        }
    };
    ValidatorController.prototype.SetMaxLengthErrorMessage = function (control) {
        this.maxLengthDesc = 'input length needs to be shorter';
        if (control) {
            var maxlength = control.getAttribute('maxlength') || control.getAttribute('ng-maxlength');
            if (maxlength) {
                this.maxLengthDesc = 'maximum input length is ' + maxlength + ' characters';
            }
        }
    };
    ValidatorController.prototype.SetPatternMismatchMessage = function () {
        if (!this.mismatch) {
            this.mismatch = 'input pattern mismatched.';
        }
    };
    return ValidatorController;
}());
ValidatorController.$inject = [];
var ValidatorComponent = (function () {
    function ValidatorComponent() {
        this.template = require('./validationMessage.html');
        this.bindings = {
            input: '=',
            title: '@',
            mismatch: '@'
        };
        this.controller = ValidatorController;
        this.controllerAs = 'me';
    }
    return ValidatorComponent;
}());
ryanValidator.component('validationMessage', new ValidatorComponent());
exports.default = (ryanValidator.name);
