﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NetCoreExercise.Forum.Models
{
    public class RedisData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
