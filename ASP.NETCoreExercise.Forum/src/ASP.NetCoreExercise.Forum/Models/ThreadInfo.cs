﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NetCoreExercise.Forum.Models
{
    public class ThreadInfo
    {
        [Key]
        public int ThreadId { get; set; }
        public string ThreadTitle { get; set; }
        public string ThreadContent { get; set; }
    }
}
