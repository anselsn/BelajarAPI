﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NetCoreExercise.Forum.Model
{
    public class User
    {
        [Required]
        [StringLength(20, ErrorMessage = "5 - 20", MinimumLength = 5)]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
