﻿using ASP.NetCoreExercise.Forum.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NetCoreExercise.Forum.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        { 
            
        }
        public DbSet<ThreadInfo> Threads { get; set; }
        //DbSet<User> Users { get; set; }
    }
}
