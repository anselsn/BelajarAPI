﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using ASP.NetCoreExercise.Forum.Services;
using ASP.NetCoreExercise.Forum.Model;
using System.ComponentModel.DataAnnotations;
using ASP.NetCoreExercise.Forum.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.NetCoreExercise.Forum.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserService UserService;

        public LoginController(UserService UserService)
        {
            this.UserService = UserService;
        }

        // GET: /<controller>/
        public IActionResult Login()
        {
            //var item = HttpContext.Items["Message"];

            //return Ok(item);
            var users = new User();
            users.Username = Request.Cookies["username"];

            return View(users);
        }
        [HttpPost]
        public IActionResult ValidateLogin(User user)
        {
            if (ModelState.IsValid)
            {
                if (!user.Username.Equals("admin"))
                {
                    ModelState.AddModelError("Username", "Wrong Username");
                    return View("Login", user);
                }
                else
                {
                    Response.Cookies.Append(user.Username, user.Username);
                    Response.Cookies.Append(user.Password, user.Password);
                    return RedirectToAction("Thread", "Thread");
                }                
            }                           
            return View("Login");

            
        }
    }
}
