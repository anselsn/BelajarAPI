﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASP.NetCoreExercise.Forum.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.NetCoreExercise.Forum.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class ThreadController : Controller
    {
        private readonly ApplicationContext _ac;
        public ThreadController(ApplicationContext ac)
        {
            _ac = ac;
        }

        // GET: /<controller>/
        public IActionResult Thread()
        {
            var ThreadList = _ac.Threads.ToList();
            return View(ThreadList);
        }

        public IActionResult ViewThread(int id)
        {
            var Thread = _ac.Threads.FirstOrDefault(Q=> Q.ThreadId == id);
            return View(Thread);
        }

        public IActionResult CreateThread()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateThread(ThreadInfo thread)
        {
            _ac.Threads.Add(thread);
            _ac.SaveChanges();
            return RedirectToAction("Thread");
        }


    }
}
