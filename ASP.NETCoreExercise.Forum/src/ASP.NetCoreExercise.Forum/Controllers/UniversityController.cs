﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using ASP.NetCoreExercise.Forum.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.NetCoreExercise.Forum.Controllers
{
    [Route("api/v1/[controller]")]
    public class UniversityController : Controller
    {
        public UniversityController(IDistributedCache cache)
        {
            this.Cache = cache;
        }

        private readonly IDistributedCache Cache;

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {

            //var value = Cache.GetString("Hi");
            //return Ok(value);
            return Ok();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var value = Cache.GetObject<List<RedisData>>(id);
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]RedisData redisData)
        {
            Cache.SetString(redisData.Key, redisData.Value, new DistributedCacheEntryOptions {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(10)
            });            
            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]string value)
        {
            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok();
        }
    }
}
