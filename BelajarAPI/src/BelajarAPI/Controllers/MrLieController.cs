﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BelajarAPI.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BelajarAPI.Controllers
{
    [Route("api/v1/[controller]")]
    public class MrLieController : Controller
    {
        //Inisialisasi DI
        private readonly List<Menu> Menus;

        public MrLieController(List<Menu> menus)
        {
            Menus = menus;
            //Inisialisasi Data List
            if (Menus.Count == 0)
            {
                Menus.Add(new Menu
                {
                    Id = 1,
                    Name = "Karage Melted Cheese",
                    Price = 17000
                });
                Menus.Add(new Menu
                {
                    Id = 2,
                    Name = "Karage Spicy Melted Cheese",
                    Price = 10000
                });
            }
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(Menus);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var x = Menus.FirstOrDefault(Q => Q.Id == id);
            return Ok(x);
        }

        // POST api/values

        //Default
        
        [HttpPost]
        //Route menggunakan HTTPPost http://localhost:35639/api/v1/MrLie/AddMenu
        //[HttpPost("AddMenu")]
        //Add Menu menggunakan Route http://localhost:35639/api/v1/MrLie/AddMenu (Harus pake [HttpPost])
        //[Route("AddMenu")]
        public IActionResult Post([FromBody]Menu menu)
        {
            menu.Id = Menus.Max(Q => Q.Id) + 1;
            Menus.Add(menu);
            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]string value)
        {
            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok();
        }
    }
}
