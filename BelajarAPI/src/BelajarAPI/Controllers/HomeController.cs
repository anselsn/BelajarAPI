﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BelajarAPI.Models;

namespace BelajarAPI.Controllers
{
    public class HomeController : Controller
    {
        private readonly List<Menu> Menus;

        public HomeController(List<Menu> menus)
        {
            Menus = menus;
            //Inisialisasi Data List
            if (Menus.Count == 0)
            {
                Menus.Add(new Menu
                {
                    Id = 1,
                    Name = "Karage Melted Cheese",
                    Price = 17000
                });
                Menus.Add(new Menu
                {
                    Id = 2,
                    Name = "Karage Spicy Melted Cheese",
                    Price = 10000
                });
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpPost]
        public IActionResult PostNew(Menu menu)
        {
            menu.Id = Menus.Max(Q => Q.Id) + 1;
            Menus.Add(menu);
            return View("Index");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
